<?php

	class Message extends AppModel {
		public $validate = array(
	        'title' => array(
	            'required' => array(
	            		'rule' => 'notEmpty',
	            		'message' => 'Field Missing'
	            	)
	        ),
	        'name' => array(
	             'required' => array(
	            		'rule' => 'notEmpty',
	            		'message' => 'Field Missing'
	            	)
	        ),
	        'comment' => array(
	             'required' => array(
	            		'rule' => 'notEmpty',
	            		'message' => 'Field Missing'
	            	)
	        )
	    );

	}

?>