<?php

	class MessagesController extends AppController {
	    public $components = array('RequestHandler','Session');
		public $helpers = array('Js', 'Html','Form');

	    public $paginate = array(
	        'limit' => 3,
	        'order' => array(
	            'Message.created' => 'desc'
	        )
	    );

	    public function index() {
	    	//set page title for layout
	    	$this->set('pageTitle','Evolutia guestbook-o-tron');

	    	//get all orders and order by creation date with newest first. also pass to the view
	    	$data = $this->paginate('Message');
    		$this->set('messages', $data);

    		if($this->request->is('post')) {
	    		$this->Message->create();
	    		if($this->Message->save($this->request->data)) {
	    			$this->Session->setFlash('Your comment has been saved.');
	    			$this->redirect(array('action' => 'index'));
	    		} else {
	    			$this->Session->setFlash('Unable to add your comment.');
	    		}
	    	}

    	}
	}

?>