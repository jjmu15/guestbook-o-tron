<?php
	if($messages) {
		$this->Paginator->options(array(
		    'update' => '#content',
		    'evalScripts' => true,
		    'before' => $this->Js->get('#busy-indicator')->effect('fadeIn', array('buffer' => false)),
		    'complete' => $this->Js->get('#busy-indicator')->effect('fadeOut', array('buffer' => false)),
		));
	?>
	<?php echo $this->Html->image('indicator.gif', array('id' => 'busy-indicator', 'alt' => 'loading')); ?>
	 <?php foreach ($messages as $message) { ?>
	 	<article class="comment roundedCorners">
	 		<?php echo $this->Html->image('avatars/defaultAvatar.png',
	 			array(
	 			'class' => 'avatar',
	 			'alt' => $message['Message']['name'],
	 			'title' => $message['Message']['name']
	 			));
	 		?>
		 	<h3>
		 		<?php echo $message['Message']['title']; ?>
			 </h3>
			 <p class="info"><span>Posted By <?php echo $message['Message']['name']; ?> on <?php echo date('d/m/Y', strtotime($message['Message']['created'])); ?></span></p>
			 <p><?php echo $message['Message']['comment']; ?></p>

	 	</article>
	 <?php } //end foreach messages ?>

	<div class="pages">
		<?php echo $this->Paginator->numbers(array('separator' => '')); ?>
	</div>
	<?php } else { ?>
		<article class="comment roundedCorners">
			<h3>There are currently no comments</h3>
		</article>
	<?php } ?>

<aside>
	<h2><span>Add your entry</span></h2>

	<div class="addMessage">
		<?php
			$data = $this->Js->get('#MessageIndexForm')->serializeForm(array('isForm' => true, 'inline' => true));
		    $this->Js->get('#MessageIndexForm')->event(
		          'submit',
		          $this->Js->request(
		            array('action' => 'save'),
		            array(
		                    'update' => '#content',
		                    'data' => $data,
		                    'async' => true,
		                    'dataExpression'=>true,
		                    'method' => 'POST'
		                )
		            )
		        );

			echo $this->Form->create('Message', array('novalidate' => 'novalidate'));
			echo $this->Form->input('title', array('label' => 'Title of Comment', 'class' => 'text'));
			echo $this->Form->input('name', array('label' => 'Your Name', 'class' => 'text'));
			echo $this->Form->input('comment', array('rows' => '3', 'label' => 'Your Comment', 'class' => 'text'));
			echo $this->Form->end('Submit', array('class' => 'submit'));
		?>
	</div>

</aside>

<?php echo $this->Js->writeBuffer(); // Write cached scripts ?>
